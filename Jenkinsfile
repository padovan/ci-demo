#!/usr/bin/env groovy

def version = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')
def storage = '/home/jenkins'
def image = "debian-${version}.img"
def image_url = "http://juma:8000/${image}"

node {
  stage ('Checkout SCM') {
    checkout scm
  }

  stage ('Build Image') {
    sh (script: "docker run --device /dev/kvm -w ${storage} -v ${storage}:${storage} -v ${workspace}:${workspace} --security-opt label:disable -t docker-registry.collabora.com/debos:latest debos -t image:${image} ${workspace}/debian.yaml")
  }
}

def hook
node {
  stage ('Submit Test Job') {
    hook = registerWebhook()
    sh (script: "lqa submit -t callback_url:${hook.getURL()} -t image:${image_url} qemu-amd64-debian.yaml")
  }
}

def result
stage("Wait for LAVA") {
  result = waitForWebhook hook
}

stage("Parsing results") {
  node {
    checkout scm

    writeFile(file: "results.json", text: result)

    sh(script: "python convert-results results.json results.xml")   

    junit("results.xml")
  }
}